package com.company;

public class JavascriptDeveloper extends Worker implements FrontendDeveloper{

    public JavascriptDeveloper(String name) {
        super(name);
    }

    @Override
    public void develop() {
        writefront();
    }

    @Override
    public void work() {
        develop();
    }

    @Override
    public void writefront() {
        System.out.println("My name is " + getName() + " , I'm writting back in Javascript!");
    }

}