package com.company;

public interface Developer extends Person{
    void develop();
}
