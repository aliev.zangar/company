package com.company;

public class Main {

    public static void main(String[] args) {
        Company company = new Company();

        company.addEmployee(new JavaDeveloper("Johny"));
        company.addEmployee(new JavascriptDeveloper("Robert"));
        company.addEmployee(new FullStackDeveloper("Andrey"));

        company.startwork();
    }
}
