package com.company;

public class FullStackDeveloper extends Worker implements BackendDeveloper, FrontendDeveloper{
    public FullStackDeveloper(String name) {
        super(name);
    }

    @Override
    public void writeBack() {
        System.out.println("My name is " + getName() + " , I'm writting back in C++!");
    }

    @Override
    public void writefront() {
        System.out.println("My name is " + getName() + " , I'm writting back in CSS!");

    }

    @Override
    public void develop() {
        writeBack();
        writefront();
    }

    @Override
    public void work() {
        develop();
    }

}
