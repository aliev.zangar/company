package com.company;

public interface BackendDeveloper extends Developer{
    void writeBack();
}
