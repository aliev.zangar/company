package com.company;

public class JavaDeveloper extends Worker implements BackendDeveloper{

    public JavaDeveloper(String name){
        super(name);
    }

    @Override
    public void work() {
        develop();
    }

    @Override
    public void writeBack() {
        System.out.println("My name is " + getName() + " , I'm writting back in Java!");
    }

    @Override
    public void develop() {
        writeBack();
    }
}
